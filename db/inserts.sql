INSERT INTO integrantes (nombre, apellido, matricula, orden, activo) VALUES
('Alexis Adrián', 'Duarte Flecha', 'Y17825', 1, 1),
('Lennys Monserrth', 'Cantero Franco', 'Y26426', 2, 1),
('Gabriel', 'Garcete', 'Y23865', 3, 1),
('Elvio', 'Martinez', 'Y12858', 4, 1),
('Marco', 'Ortigoza Colman', 'UG0507', 5, 1);

INSERT INTO tipoMedia (nombre, orden, activo) VALUES
('Imagen', 1, 1),
('Youtube', 2, 1),
('Dibujo', 3, 1);

INSERT INTO media (tipoMedia, url, src, matricula, titulo, alt, orden, activo) VALUES
(1, NULL, '/assets/images/imagen-personalidad.jpg', 'Y17825', 'Imagen favorita', 'Imagen de un Golden que me representa', 1, 1),
(2, 'https://www.youtube.com/embed/U1ivmi3_IeI?si=9QjM1bJzS3uUpMYk', NULL, 'Y17825', 'Video favorito', 'Video favorito', 2, 1),
(3, NULL, '/assets/images/dibujo-sistema-solar.png', 'Y17825', 'Dibujo favorito', 'Dibujo favorito', 3, 1),

(1, NULL, '/assets/images/saturno.jpg', 'UG0507', 'Imagen favorita', 'Imagen favorita', 4, 1),
(2, 'https://www.youtube.com/embed/RW75cGvO5xY', NULL, 'UG0507', 'Video favorito', 'Video favorito', 5, 1),
(3, NULL, '/assets/images/TERERE.png', 'UG0507', 'Dibujo favorito', 'Dibujo favorito', 6, 1),

(1, NULL, '/assets/images/melissa.jpg', 'Y12858', 'Imagen favorita', 'Imagen favorita', 7, 1),
(2, 'https://www.youtube.com/embed/VhoHnKuf-HI', NULL, 'Y12858', 'Video favorito', 'Video favorito', 8, 1),
(3, NULL, '/assets/images/dibujo-Elvio.png', 'Y12858', 'Dibujo favorito', 'Dibujo favorito', 9, 1),

(1, NULL, '/assets/images/messi_pou.jpeg', 'Y23865', 'Imagen favorita', 'Imagen favorita', 10, 1),
(2, 'https://www.youtube.com/embed/B4LvDiIi128?rel=0', NULL, 'Y23865', 'Video favorito', 'Video favorito', 11, 1),
(3, NULL, '/assets/images/paint_garcete.jpg', 'Y23865', 'Dibujo favorito', 'Dibujo favorito', 12, 1),

(1, NULL, '/assets/images/imagen-56.jpeg', 'Y26426', 'Imagen favorita', 'Imagen favorita', 13, 1),
(2, 'https://www.youtube.com/embed/vi6-oF3tsPs?si=AdywmEQJk-ewbUQg', NULL, 'Y26426', 'Video favorito', 'Video favorito', 14, 1),
(3, NULL, '/assets/images/imagen-57.png', 'Y26426', 'Dibujo favorito', 'Dibujo favorito', 15, 1);

INSERT INTO home (nombre, titulo, src, alt) VALUES
('FSOCIETY', 'Bienvenidos al grupo', '/assets/images/logo.jpeg', 'Grupo FSOCIETY');

INSERT INTO usuarios (email, contrasenha, super_usuario, matricula) VALUES
('admin@gmail.com', '$2b$10$nlSz1AMKAiF.czm5dNtaJe5j8FAltTFezQfnewJQe2rn8Vh1wdy9m', 1, 'Y17825');
