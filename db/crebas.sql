CREATE TABLE IF NOT EXISTS "integrantes" (
    "matricula" TEXT NOT NULL UNIQUE,
    "nombre" TEXT NOT NULL,
    "apellido" TEXT NOT NULL,
    "orden" INTEGER,
    "activo" INTEGER,
    PRIMARY KEY("matricula")
);

CREATE TABLE IF NOT EXISTS "tipoMedia" (
    "id" INTEGER NOT NULL UNIQUE,
    "nombre" TEXT NOT NULL,
    "orden" INTEGER,
    "activo" INTEGER,
    PRIMARY KEY("id")
);

CREATE TABLE IF NOT EXISTS "media" (
    "id" INTEGER NOT NULL UNIQUE,
    "url" TEXT,
    "src" TEXT,
    "titulo" TEXT NOT NULL,
    "alt" TEXT,
    "tipoMedia" INTEGER,
    "matricula" TEXT,
    "orden" INTEGER,
    "activo" INTEGER,
    PRIMARY KEY("id"),
    FOREIGN KEY ("tipoMedia") REFERENCES "tipoMedia"("id")
    ON UPDATE NO ACTION ON DELETE NO ACTION,
    FOREIGN KEY ("matricula") REFERENCES "integrantes"("matricula")
    ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS "home" (
    "id" INTEGER NOT NULL UNIQUE,
    "nombre" TEXT NOT NULL,
    "titulo" TEXT NOT NULL,
    "src" TEXT NOT NULL,
    "alt" TEXT NOT NULL,
    PRIMARY KEY("id")
);

CREATE TABLE IF NOT EXISTS "usuarios" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "email" TEXT NOT NULL,
    "contrasenha" TEXT NOT NULL,
    "super_usuario" INTEGER NOT NULL,
    "matricula" TEXT NOT NULL,
    FOREIGN KEY ("matricula") REFERENCES "integrantes"("matricula")
    ON UPDATE NO ACTION ON DELETE NO ACTION
);
