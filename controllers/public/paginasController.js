const {getIntegrante, getAllMatriculas, getByMatricula} = require("../../models/integrante.model");
const {getTipoMedia} = require("../../models/tipoMedia.model");
const {getMediaByMatricula} = require("../../models/media.model");
const PublicModel = require("../../models/public.model");

const PaginasController = {
    index: async (req, res) => {
        const integrantes = await getIntegrante();
        const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);
        const home = await PublicModel.getAllHome();
        res.render("index", {
            home: home[0],
            integrantes: primerNombreIntegrantes,
            isHome: true
        });
    },

    indexWordCloud: async (req, res) => {
        const integrantes = await getIntegrante();
        const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);
        res.render("word_cloud", {
            integrantes: primerNombreIntegrantes
        });
    },

    indexCurso: async (req, res) => {
        const integrantes = await getIntegrante();
        const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);
        res.render("curso", {
            integrantes: primerNombreIntegrantes
        });
    },

    indexIntegranteMatricula: async (req, res, next) => {
        const matriculas = (await getAllMatriculas()).map(obj => obj.matricula);
        const tipoMedia = await getTipoMedia();
        const matricula = req.params.matricula;
        const integrantes = await getIntegrante();
        const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);

        if (matriculas.includes(matricula)) {
            const integranteFilter = await getByMatricula(matricula);
            const mediaFilter = await getMediaByMatricula(matricula);
            res.render('integrante', {
                integrante: integranteFilter,
                tipoMedia: tipoMedia,
                media: mediaFilter,
                integrantes: primerNombreIntegrantes
            });
        } else {
            next();
        }
    }
}

// Función para mostrar solo el primer nombre de los integrantes
function obtenerPrimerNombre(integrantes) {
    return integrantes.map(integrante => {
        const primerNombre = integrante.nombre.split(" ")[0];
        return {...integrante, nombre: primerNombre};
    })
}

module.exports = PaginasController;
