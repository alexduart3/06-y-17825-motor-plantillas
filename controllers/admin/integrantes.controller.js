const integranteStoreSchema = require("../../validators/integrantes/create.integrante")
const integranteUpdateSchema = require("../../validators/integrantes/edit.integrante");
const IntegranteModel = require("../../models/integrante.model");
const MediaModel = require("../../models/media.model");

const IntegrantesController = {
    index: async function (req, res) {
        try {
            const integrantes = await IntegranteModel.getAll(req);
            res.render("admin/integrantes/index", {
                integrantes: integrantes
            });
        } catch (error) {
            res.redirect("/admin/integrante/listar?error=" + encodeURIComponent('¡Error al obtener los integrantes!'));
        }
    },

    create: function (req, res) {
        res.render("admin/integrantes/crearIntegrante", {
            matricula: req.query.matricula,
            nombre: req.query.nombre,
            apellido: req.query.apellido
        });
    },

    store: async function (req, res) {
        const {error} = integranteStoreSchema.validate(req.body);
        const matricula = req.body.matricula;

        if (error) {
            const errorMessages = error.details.map(detail => detail.message);
            res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent(errorMessages.join(';'))}&matricula=${encodeURIComponent(req.body.matricula)}&nombre=${encodeURIComponent(req.body.nombre)}&apellido=${encodeURIComponent(req.body.apellido)}`);
        } else if (await IntegranteModel.getByField('integrantes', 'matricula', matricula)) {
            res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent('¡Ya existe un integrante con la matrícula que has introducido!')}`);
        } else {
            try {
                await IntegranteModel.create(req.body);
                res.redirect(`/admin/integrantes/listar?success=${encodeURIComponent('¡Integrante creado correctamente!')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    },

    update: async function (req, res) {
        const {error} = integranteUpdateSchema.validate(req.body);
        const matricula = req.params.matricula;

        if (error) {
            const errorMessages = error.details.map(detail => detail.message);
            res.redirect(`/admin/integrantes/edit/${matricula}?error=${encodeURIComponent(errorMessages.join(';'))}`);
        } else {
            try {
                await IntegranteModel.update(req.body, matricula);
                res.redirect("/admin/integrantes/listar?success=" + encodeURIComponent('¡Integrante actualizado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al actualizar al integrante!'));
            }
        }
    },

    edit: async function (req, res) {
        const matricula = req.params.matricula;
        try {
            const integrante = await IntegranteModel.getById(matricula);
            res.render("admin/integrantes/editarIntegrante", {
                integrante: integrante
            });
        } catch (err) {
            console.log(err);
            res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al obtener al integrante!'));
        }
    },

    destroy: async function (req, res) {
        const matricula = req.params.matricula;
        const matriculaAsociada = await MediaModel.getByField('media', 'matricula', matricula);

        if (matriculaAsociada) {
            res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡No se puede eliminar el integrante porque tiene media asociada!'));
        } else {
            try {
                await IntegranteModel.delete(matricula);
                res.redirect("/admin/integrantes/listar?success=" + encodeURIComponent('¡Integrante eliminado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
        }
    },

    /*Serán métodos implementados en el futuro*/
    /*show: function () {},*/
}

module.exports = IntegrantesController;
