const fs = require('fs').promises;
const mediaStoreSchema = require('../../validators/media/create.media');
const mediaUpdateSchema = require('../../validators/media/edit.media');
const MediaModel = require("../../models/media.model");
const TipoMediaModel = require("../../models/tipoMedia.model");
const IntegranteModel = require("../../models/integrante.model");

const mediaController = {
    index: async function (req, res) {
        try {
            const media = await MediaModel.getAll(req);
            const integrantes = await IntegranteModel.getIntegrante();
            const tipoMedia = await TipoMediaModel.getTipoMedia();
            res.render("admin/media/index", {
                media: media,
                integrantes: integrantes,
                tipoMedia: tipoMedia,
            });
        } catch (error) {
            console.log(error);
            res.redirect("/admin/media/listar?error=" + encodeURIComponent('¡Error al obtener los medias'));
        }
    },

    create: async function (req, res) {
        const integrantes = await IntegranteModel.getIntegrante();
        const tipoMedia = await TipoMediaModel.getTipoMedia();
        res.render("admin/media/crearMedia", {
            integrantes: integrantes,
            tipoMedia: tipoMedia,
            tipoMediaSeleccionado: req.query.tipoMedia,
            url: req.query.url,
            titulo: req.query.titulo,
            alt: req.query.alt,
            integranteSeleccionado: req.query.integrante,
        });
    },

    store: async function (req, res) {
        const {error}  = mediaStoreSchema.validate(req.body);

        if (error) {
            const errorMessages = error.details.map(detail => detail.message);
            res.redirect(`/admin/media/crear?error=${encodeURIComponent(errorMessages.join(';'))}
            &tipoMedia=${encodeURIComponent(req.body.tipoMedia)}
            &url=${encodeURIComponent(req.body.url)}
            &titulo=${encodeURIComponent(req.body.titulo)}
            &alt=${encodeURIComponent(req.body.alt)}
            &integrante=${encodeURIComponent(req.body.integrante)}`);
        } else {
            let srcPath = '';
            if (req.file) {
                var tpm_path = req.file.path;
                var destino = "public/assets/images/" + req.file.originalname;
                try {
                    await fs.rename(tpm_path, destino);
                    srcPath = "/assets/images/" + req.file.originalname;
                } catch (err) {
                    return res.sendStatus(500);
                }
            }
            try {
                await MediaModel.create(req.body, srcPath);
                res.redirect(`/admin/media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/media/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    },

    update: async function (req, res) {
        const {error} = mediaUpdateSchema.validate(req.body);
        const id = parseInt(req.params.id);
        const existingMedia = await MediaModel.getById(id);

        let errores = [];

        if (existingMedia.src && req.body.url) {
            errores.push('¡No puedes agregar una URL porque ya existe un SRC!');
        }

        if (existingMedia.url && req.file) {
            errores.push('¡No puedes agregar un SRC porque ya existe una URL!');
        }

        if (error || errores.length > 0) {
            const errorMessages = error? error.details.map(detail => detail.message) : [];
            errores = errores.concat(errorMessages);
            res.redirect(`/admin/media/edit/${id}?error=${encodeURIComponent(errores.join(';'))}`);
        } else {
            let srcPath = '';
            if (existingMedia.src) {
                srcPath = existingMedia.src;
            }
            if (req.file) {
                var tpm_path = req.file.path;
                var destino = "public/assets/images/" + req.file.originalname;
                try {
                    await fs.rename(tpm_path, destino);
                    srcPath = "/assets/images/" + req.file.originalname;
                } catch (err) {
                    return res.sendStatus(500);
                }
            }
            try {
                await MediaModel.update(req.body, id, srcPath);
                res.redirect("/admin/media/listar?success=" + encodeURIComponent('¡Media actualizado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/media/edit/" + id + "?error=" + encodeURIComponent('¡Error al actualizar el registro!'));
            }
        }
    },

    edit: async function (req, res) {
        const id = parseInt(req.params.id);
        const integrantes = await IntegranteModel.getIntegrante();
        const tipoMedia = await TipoMediaModel.getTipoMedia();
        try {
            const media = await MediaModel.getById(id);
            res.render("admin/media/editarMedia", {
                integrantes: integrantes,
                tipoMedia: tipoMedia,
                media: media
            });
        } catch (err) {
            console.log(err);
            res.redirect("/admin/media/listar?error=" + encodeURIComponent('¡Error al obtener el registro!'));
        }
    },

    destroy: async function (req, res) {
        const id = parseInt(req.params.id);
        try {
            await MediaModel.delete(id);
            res.redirect("/admin/media/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
        } catch (err) {
            console.log(err);
            res.redirect("/admin/media/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
        }
    },

    /*Serán métodos implementados en el futuro*/
    /*show: function () {},*/
}

module.exports = mediaController;
