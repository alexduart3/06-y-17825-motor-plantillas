const tipoMediaStoreSchema = require("../../validators/tipoMedia/create.tipoMedia");
const tipoMediaUpdateSchema = require("../../validators/tipoMedia/edit.tipoMedia");
const TipoMediaModel = require("../../models/tipoMedia.model");
const MediaModel = require("../../models/media.model");

const tipoMediaController = {
    index: async function (req, res) {
        try {
            const tipoMedia = await TipoMediaModel.getAll(req);
            res.render("admin/tipoMedia/index", {
                tipoMedia: tipoMedia
            });
        } catch (error) {
            res.redirect("/admin/tipoMedia/listar?error=" + encodeURIComponent('¡Error al obtener los tipos de media!'));
        }
    },

    create: function (req, res) {
        res.render("admin/tipoMedia/crearTipoMedia", {
            nombre: req.query.nombre,
        });
    },

    store: async function (req, res) {
        const {error} = tipoMediaStoreSchema.validate(req.body);

        if (error) {
            const errorMessages = error.details.map(detail => detail.message);
            res.redirect(`/admin/tipoMedia/crear?error=${encodeURIComponent(errorMessages.join(';'))}&nombre=${encodeURIComponent(req.body.nombre)}`);
        } else {
            try {
                await TipoMediaModel.create(req.body);
                res.redirect(`/admin/tipoMedia/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/tipoMedia/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    },

    update: async function (req, res) {
        const {error} = tipoMediaUpdateSchema.validate(req.body);
        const id = parseInt(req.params.id);

        if (error) {
            const errorMessages = error.details.map(detail => detail.message);
            res.redirect(`/admin/tipoMedia/edit/${id}?error=${encodeURIComponent(errorMessages.join(';'))}`);
        } else {
            try {
                await TipoMediaModel.update(req.body, id);
                res.redirect("/admin/tipoMedia/listar?success=" + encodeURIComponent('¡Tipo media actualizado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/tipoMedia/listar?error=" + encodeURIComponent('¡Error al actualizar el tipo media!'));
            }
        }
    },

    edit: async function (req, res) {
        const id = parseInt(req.params.id);
        try {
            const tipoMedia = await TipoMediaModel.getById(id);
            res.render("admin/tipoMedia/editarTipoMedia", {
                tipoMedia: tipoMedia
            });
        } catch (err) {
            console.log(err);
            res.redirect("/admin/tipoMedia/listar?error=" + encodeURIComponent('¡Error al obtener el tipo media!'));
        }
    },

    destroy: async function (req, res) {
        const id = parseInt(req.params.id);
        const idAsociado = await MediaModel.getByField('media', 'tipoMedia', id);

        if (idAsociado) {
            res.redirect(`/admin/tipoMedia/listar?error=${encodeURIComponent('¡No se puede eliminar el registro porque está siendo utilizado en la tabla media!')}`);
        } else {
            try {
                await TipoMediaModel.delete(id);
                res.redirect("/admin/tipoMedia/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/tipoMedia/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
        }
    },

    /*Serán métodos implementados en el futuro*/
    /*show: function () {},*/
}

module.exports = tipoMediaController;
