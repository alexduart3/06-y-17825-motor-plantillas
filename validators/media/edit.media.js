const Joi = require("joi");

const mediaUpdateSchema = Joi.object({
    tipoMedia:
        Joi.string()
        .required()
        .messages({
            "string.empty": "¡Debe seleccionar un tipo de media!"
        }),

    integrante:
        Joi.string()
        .required()
        .messages({
            "string.empty": "¡Debe seleccionar un integrante!"
        }),

    titulo:
        Joi.string()
        .required()
        .max(50)
        .messages({
            "string.empty": "¡El título no puede estar vacío!",
            "string.max": "¡El título debe tener una longitud máxima de 50 caracteres!"
        }),

    alt:
        Joi.string()
        .required()
        .max(80)
        .messages({
            "string.empty": "¡El alt no puede estar vacío!",
            "string.max": "¡El alt debe tener una longitud máxima de 80 caracteres!"
        }),

    url:
        Joi.string()
        .optional(),

    src:
        Joi.string()
        .optional(),

}).options({abortEarly: false});

module.exports = mediaUpdateSchema;
