const Joi = require('joi');

const mediaSchema = Joi.object({
    tipoMedia:
        Joi.string()
        .required()
        .messages({
            'string.empty': '¡Debe seleccionar un tipo de media!',
        }),

    integrante:
        Joi.string()
        .required()
        .messages({
            'string.empty': '¡Debe seleccionar un integrante!',
        }),

    titulo:
        Joi.string()
        .required()
        .max(50)
        .messages({
            'string.empty': '¡Debe introducir un título!',
            'string.max': '¡El título no puede exceder 50 caracteres!',
        }),

    alt:
        Joi.string()
        .required()
        .max(80)
        .messages({
            'string.empty': '¡Debe introducir un alt!',
            'string.max': '¡El título no puede exceder 80 caracteres!',
        }),

    url:
        Joi.string()
            .optional(),

    src:
        Joi.string()
            .optional(),

    activo:
        Joi.required()
        .messages({
            'any.required': '¡Debe seleccionar un estado (activo o inactivo)!',
        }),
}).options({abortEarly: false});

module.exports = mediaSchema;
