const Joi = require("joi");

const tipoMediaUpdateSchema = Joi.object({
    nombre:
        Joi.string()
        .required()
        .max(50)
        .messages({
            "string.empty": "¡El nombre no puede estar vacío!",
            "string.max": "¡El nombre debe tener una longitud máxima de 50 caracteres!"
        })
}).options({abortEarly: false});

module.exports = tipoMediaUpdateSchema;
