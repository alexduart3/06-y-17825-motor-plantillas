const Joi = require('joi');

const integranteUpateSchema = Joi.object({
    nombre:
        Joi.string()
        .required()
        .max(50)
        .messages({
            'string.empty': '¡El nombre no puede estar vacío!',
            'string.max': '¡El nombre debe tener una longitud máxima de 50 caracteres!',
        }),

    apellido:
        Joi.string()
        .required()
        .max(50)
        .messages({
            'string.empty': '¡El apellido no puede estar vacío!',
            'string.max': '¡El apellido debe tener una longitud máxima de 50 caracteres!',
        }),
}).options({abortEarly: false});

module.exports = integranteUpateSchema;
