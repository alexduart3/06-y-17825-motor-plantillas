const {db} = require("../db/conexion");

const PublicModel = {
    getAllHome() {
        return new Promise((resolve, reject) => {
            db.all("SELECT * FROM home", (error, home) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(home);
                }
            });
        });
    },
}

module.exports = PublicModel;
