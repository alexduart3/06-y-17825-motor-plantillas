const {db, getLastOrder} = require("../db/conexion");

const IntegranteModel = {
    //Obtener todos los registros - filtros opcionales de búsqueda deben estar disponibles por cada campo de la tabla
    async getAll(req) {
        let query = "SELECT * FROM integrantes WHERE activo = 1";
        let queryParams = [];

        for (const prop in req.query["s"]) {
            if (req.query["s"][prop]) {
                query += ` AND ${prop} LIKE?`;
                queryParams.push(`%${req.query["s"][prop]}%`);
            }
        }

        query += " ORDER BY orden";
        return new Promise((resolve, reject) => {
            db.all(query, queryParams, (error, integrantes) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(integrantes);
                }
            });
        });
    },

    // Obtener un registro por ID
    getById(matricula) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM integrantes WHERE matricula = ?`, [matricula], (error, integrante) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(integrante);
                }
            });
        });
    },

    // Crear registro
    async create(req) {
        const lastOrder = await getLastOrder('integrantes');
        const newOrder = lastOrder + 1;

        return new Promise((resolve, reject) => {
            db.run("insert into integrantes (orden, matricula, nombre, apellido, activo) values (?, ?, ?, ?, ?)",[newOrder, req.matricula, req.nombre, req.apellido, req.activo], (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

    // Actualizar registro
    update(req, matricula) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE integrantes SET nombre = ?, apellido = ? WHERE matricula = ?", [req.nombre, req.apellido, matricula], error => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

    // Borrar registro
    delete(matricula) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE integrantes SET activo = 0 WHERE matricula = ?", [matricula], error => {
               if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

    getByField(tabla, clave, valor) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT 1 FROM ${tabla} WHERE ${clave} = ?`, [valor], (error, row) => {
                if (error) {
                    reject(error);
                } else {
                    if (row) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }
            });
        });
    },

    getIntegrante() {
        return new Promise((resolve, reject) => {
            db.all("SELECT * FROM integrantes WHERE activo = 1 ORDER BY nombre", (error, integrante) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(integrante);
                }
            });
        });
    },

    getAllMatriculas() {
        return new Promise((resolve, reject) => {
            db.all("SELECT matricula FROM integrantes WHERE activo = 1 ORDER BY orden", (error, integrante) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(integrante);
                }
            });
        });
    },

    getByMatricula(matricula) {
        return new Promise((resolve, reject) => {
            db.all(`SELECT * FROM integrantes WHERE activo = 1 AND matricula = ?`, [matricula], (error, integrante) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(integrante);
                }
            });
        });
    }
}

module.exports = IntegranteModel;
