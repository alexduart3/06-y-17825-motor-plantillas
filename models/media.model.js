const {db, getLastOrder, getLastId, run} = require("../db/conexion");

const MediaModel = {
    //Obtener todos los registros - filtros opcionales de búsqueda deben estar disponibles por cada campo de la tabla
    async getAll(req) {
        let baseQuery = `
        SELECT media.*, tipoMedia.nombre as tipoMediaNombre, integrantes.nombre as integranteNombre, integrantes.apellido as integranteApellido
        FROM media
        LEFT JOIN tipoMedia ON media.tipoMedia = tipoMedia.id
        LEFT JOIN integrantes ON media.matricula = integrantes.matricula
        WHERE media.activo = 1`;

        let queryParams = [];

        if (req.query['s']) {
            if (req.query['s']['integrante']) {
                baseQuery += ` AND integrantes.matricula LIKE?`;
                queryParams.push(`%${req.query['s']['integrante']}%`);
            }

            if (req.query['s']['tipoMedia']) {
                baseQuery += ` AND tipoMedia.id LIKE?`;
                queryParams.push(`%${req.query['s']['tipoMedia']}%`);
            }

            if (req.query['s']['titulo']) {
                baseQuery += ` AND media.titulo LIKE?`;
                queryParams.push(`%${req.query['s']['titulo']}%`);
            }
        }

        baseQuery += " ORDER BY media.orden";

        return new Promise((resolve, reject) => {
            db.all(baseQuery, queryParams, (error, media) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media);
                }
            });
        });
    },

    // Obtener un registro por ID
    getById(id) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM media WHERE id = ?`, [id], (error, media) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media);
                }
            });
        });
    },

    // Crear registro
    async create(req, srcPath) {
        const lastOrder = await getLastOrder('media');
        const newOrder = lastOrder + 1;
        const lastId = await getLastId('media');
        const newId = lastId + 1;

        return new Promise((resolve, reject) => {
            db.run("insert into media (id, url, src, titulo, alt, tipoMedia, matricula, orden, activo) values (?, ?, ?, ?, ?, ?, ?, ?, ?)", [newId, req.url, srcPath, req.titulo, req.alt, req.tipoMedia, req.integrante, newOrder, req.activo,], (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

    // Actualizar registro
    update(req, id, srcPath) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE media SET url = ?, src = ?, titulo = ?, alt = ?, tipoMedia = ?, matricula = ? WHERE id = ?", [req.url, srcPath, req.titulo, req.alt, req.tipoMedia, req.integrante, id], error => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

    // Borrar registro
    delete(id) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE media SET activo = 0 WHERE id = ?", [id], error => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

    getByField(tabla, clave, valor) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT 1 FROM ${tabla} WHERE ${clave} = ?`, [valor], (error, row) => {
                if (error) {
                    reject(error);
                } else {
                    if (row) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }
            });
        });
    },

    getMediaByMatricula(matricula) {
        return new Promise((resolve, reject) => {
            db.all(`SELECT * FROM media WHERE activo = 1 AND matricula = ? ORDER BY orden`, [matricula], (error, media) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media);
                }
            });
        });
    },
}

module.exports = MediaModel;
