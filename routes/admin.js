const express = require("express");
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: "./public/assets/images"});
const fileUpload = upload.single('src');
const session = require("express-session");
const AuthMiddleware = require('../middlewares/auth.middleware');
const IntegrantesControllerAdmin = require('../controllers/admin/integrantes.controller');
const TipoMediaControllerAdmin = require('../controllers/admin/tipoMedia.controller');
const MediaControllerAdmin = require('../controllers/admin/media.controller');
const InicioControllerAdmin = require('../controllers/admin/inicio.controller');


router.use(session({
    name: "session",
    secret: "clave-aleatoria-y-secreta",
    resave: false,
    saveUninitialized: true
}));

router.use((req, res, next) => {
    res.locals.isAuthenticated = !!req.session.userId;
    next();
});

router.use(AuthMiddleware.validateAuth);

router.get("/",  InicioControllerAdmin.index);

// Integrantes
router.get("/integrantes/listar", IntegrantesControllerAdmin.index);

router.get("/integrantes/crear", IntegrantesControllerAdmin.create);

router.post("/integrantes/create", IntegrantesControllerAdmin.store);

router.post("/integrantes/delete/:matricula", IntegrantesControllerAdmin.destroy);

router.get("/integrantes/edit/:matricula", IntegrantesControllerAdmin.edit);

router.post("/integrantes/update/:matricula", IntegrantesControllerAdmin.update);

// Tipo Media
router.get("/tipoMedia/listar", TipoMediaControllerAdmin.index);

router.get("/tipoMedia/crear", TipoMediaControllerAdmin.create);

router.post("/tipoMedia/create", TipoMediaControllerAdmin.store);

router.post("/tipoMedia/delete/:id", TipoMediaControllerAdmin.destroy);

router.get("/tipoMedia/edit/:id", TipoMediaControllerAdmin.edit);

router.post("/tipoMedia/update/:id", TipoMediaControllerAdmin.update);

// Media
router.get("/media/listar", MediaControllerAdmin.index);

router.get("/media/crear", MediaControllerAdmin.create);

router.post("/media/create", fileUpload, MediaControllerAdmin.store);

router.post("/media/delete/:id", MediaControllerAdmin.destroy);

router.get("/media/edit/:id", MediaControllerAdmin.edit);

router.post("/media/update/:id", fileUpload, MediaControllerAdmin.update);

module.exports = router;
