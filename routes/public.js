const express = require("express");
const router = express.Router();
require("dotenv").config();
const PaginasControllerPublic = require('../controllers/public/paginasController');

router.use((req, res, next) => {
    res.locals.repositorio = process.env.REPOSITORIO;
    res.locals.nombre = process.env.NOMBRE;
    res.locals.materia = process.env.MATERIA;
    res.locals.matricula = process.env.MATRICULA;
    next();
});

router.get("/", PaginasControllerPublic.index);

router.get("/word_cloud/", PaginasControllerPublic.indexWordCloud);

router.get("/curso/", PaginasControllerPublic.indexCurso);

router.get("/:matricula", PaginasControllerPublic.indexIntegranteMatricula);

module.exports = router;
